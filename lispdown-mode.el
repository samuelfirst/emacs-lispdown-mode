;;; lispdown-mode.el --- Lispdown Mode -*- lexical-binding: t; -*-
;;; Code:
(defvar lispdown-mode-hook nil
  "User-defined function hook for lispdown mode")

(defvar lispdown-tab-width 2
  "Number of columns to indent each sub-level")

(defvar lispdown-mode-map
  (let ((map (make-keymap)))
    (define-key map "" nil))
  "Keymap for lispdown mode")

;;; Add the mode to the auto-mode-list so it's automatically applied for ld files
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.lispdown\\'" . lispdown-mode))

;;; Define list of keywords for syntax highlighting purposes
;; Efficient regex generated with `(regexp-opt '(<keyword strings>))`.
;; Regex is then bookended with \< and \> to specify that only the keyword on
;; its own should be selected.
;;
;; ::==================================================::
;; ||     Keyword      ||             Font	       ||
;; ::==================::==============================::
;; || section/template || font-lock-keyword-face       ||
;; || text/bold/slant  || font-lock-doc-markup-face    ||
;; || image/link       || font-lock-function-name-face ||
;; || list/table/code  || font-lock-warning-face       ||
;; ::==================================================::
(defconst lispdown-font-lock-keywords
  (list '("\\((\\)\\<\\(section\\|template\\)\\>" (2 font-lock-keyword-face))
	'("\\((\\)\\<\\(bold\\|\\(slan\\|tex\\)t\\)\\>"
	  (2 font-lock-doc-markup-face))
	'("\\((\\)\\<\\(image\\|link\\)\\>"  (2 font-lock-function-name-face))
	'("\\((\\)\\<\\(code\\|list\\|table\\)\\>" (2 font-lock-warning-face)))
  "Set of keywords for lispdown mode")

;;; Indent the current line according to the following rules:
;;  0. If the line is at the beginning of the buffer, indent it to 0
;;  1. Look at the previous line
;;  2. For every open paren on the previous line, increment indent by one
;;  3. For every closed paren on the previous line, decrement indent by one
;;  4. If there is a double quote ("), stop interpreting parens until a closing
;;     double quote is encountered.
;; Keycodes referenced: 40 = (, 41 = ), 34 = "
(defun lispdown-indent-line ()
  "Indent the current line as lispdown"
  (interactive)
  (beginning-of-line)
  (if (bobp) (indent-line-to 0)
    (let ((indent 0))
      (save-excursion
	(forward-line -1)
	(setq indent (current-indentation))
	(let ((line (buffer-substring-no-properties
		     (line-beginning-position) (line-end-position)))
	      (ignore nil))
	  (dotimes (i (length line))
	    (cond ((equal (aref line i) 34) (if ignore (setq ignore nil)
					      (setq ignore t)))
		  (ignore nil)
		  ((equal (aref line i) 40) (setq indent (+ indent tab-width)))
		  ((equal (aref line i) 41) (setq indent (- indent tab-width)))))
	  (if (< indent 0) (setq indent 0))))
      (indent-line-to indent))))

;;; Function to start mode
;;;###autoload
(defun lispdown-mode ()
  "Major mode for editing lispdown formatted documents"
  (interactive)
  (kill-all-local-variables)
  (set (make-local-variable 'font-lock-defaults) '(lispdown-font-lock-keywords))
  (set (make-local-variable 'indent-line-function) 'lispdown-indent-line)
  (setq tab-width lispdown-tab-width)
  (setq major-mode 'lispdown-mode)
  (setq mode-name "Lispdown")
  (run-hooks 'lispdown-mode-hooks))

(provide 'lispdown-mode)

;;; lispdown-mode.el ends here
