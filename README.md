# emacs-lispdown-mode

This is a major mode for the custom lisp dialect that I use in
the static site generator for 
[my homepage](https://gitlab.com/samuelfirst/samuelfirst.org/).

If you're reading this looking to figure out how to write emacs
major modes, I wrote this using 
[this tutorial](https://www.emacswiki.org/emacs/ModeTutorial).

All the code within this repo is GPLv3, so feel free to fork, 
redistribute, etc.

## A Note on Font Lock

The documentation on how font-lock works is arcane and sparse.  What I've found
works for applying syntax highlighting only to opening forms is to use 
sub-expressions to specify which part of the match to highlight.  Meaning that
the following will match a keyword beginning with a paren, but not the paren
itself:

```
(defconst my-keywords
	(list '("\\((\\)\\<\\(keyword\\)\\>)" (2 font-lock-keyword-face))))
```

Replacing `(2 font-lock-keyword-face)` with `(1 font-lock-keyword-face)` will
highlight the first match (the paren itself).
